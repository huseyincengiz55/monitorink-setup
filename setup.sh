#!/bin/bash


value=$(cat /etc/os-release | grep -m 1 "ID")
if [ $value == 'ID="centos"' ]; then
yum -y update
yum -y install gcc python3 python3-pip python3-devel
python3 -m pip install --upgrade pip
pip3 install psutil distro
echo '@reboot python3 /usr/src/tagent/agent.py' >> /var/spool/cron/root
else
apt-get -y update
apt-get -y install python3 python3-pip
python3 -m pip install --upgrade pip
pip3 install psutil distro --break-system-packages
echo '@reboot python3 /usr/src/tagent/agent.py' >> /var/spool/cron/crontabs/root
fi

rm -rf /usr/src/tagent/
kill -9 `pidof python3 /usr/src/tagent/agent.py`
pkill -f update.py
pkill -f agent.py

ip=$1
server_ip="server_ip.txt"

if [ -n "$ip" ]; then
echo "$1" >> ${server_ip}
else
    echo "argument error"
fi

cd /usr/src/
git clone https://huseyincengiz55@bitbucket.org/huseyincengiz55/monitorink-agent.git tagent

cat > /usr/src/tagent/tagent-update.sh <<EOFMARKER7
#!/bin/bash
cd /usr/src/tagent/
git reset --hard
git pull
kill -9 \`pidof python3 /usr/src/tagent/agent.py\`
kill -9 \`pidof python2 /usr/src/tagent/agent.py\`
pkill -f agent.py
EOFMARKER7

chmod u+x /usr/src/tagent/tagent-update.sh
echo 'nohup python3 /usr/src/tagent/agent.py >/dev/null 2>&1 &' >> /usr/src/tagent/tagent-update.sh

mv /usr/src/server_ip.txt /usr/src/tagent/
nohup python3 /usr/src/tagent/agent.py >/dev/null 2>&1 &
